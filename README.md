# Desafio WPensar

Pessoal!
Obrigado pela oportunidade!!

Dentre as várias coisas que disse que eu gostaria de ter tempo pra fazer, eu consegui:

- Melhorar um pouco a apresentação.
- Melhorar um pouco o layout.
- Expor endpoints para as entidades Produto e Lote de Compra, que são as principais no momento.
- Implementar o OAuth do Discord e Google.
- Melhorar minha compreensão sobre algumas composições do DJANGO.
- Simplificar algumas implementações.

Mas infelizmente, devido ao alto volume de atividades em que eu estou envolvido atualmente como meu trabalho atual, faculdade e empresa junior,
ainda há algumas pendências que eu ainda não tive todo tempo necessário para completar devido as minhas circunstâncias de disponibilidade principalmente por conta do meu trabalho atual, que tem consumido meu tempo além do horário comercial ..., como:

- Todos os testes possíveis.
- Usar o jinja. :(
- Alguns layouts quebrando visual, mas funcionando ...
- Contornar o erro do drf-datatables sobre ordenar @propriety 'custo' ...

Espero que o que eu consegui realizar seja satisfatório para uma avaliação positiva!!

Ty!

- python 3.7
- django 3.1
- DRF 3.12
- ... requirements.txt

###### Link do heroku https://querotrabalharnawpensar.herokuapp.com/login

###### /admin, usuario django senha django

Depois de realizar o login com o OAuth, é preciso logar no admin como django e atribuir ao usuario o grupo(único cadastrado) para ele ter as permissões de acesso aos recursos.
