from django.db import models
from django_extensions.db.models import TimeStampedModel
from django.contrib.auth.models import User


class ServicoCredencial(TimeStampedModel, models.Model):
    nome = models.CharField(max_length=50)

    class Meta:
        db_table = 'CREDENCIAL_TIPO'
        verbose_name_plural = "Serviços de Credencial"

    def __str__(self):
        return self.nome


class Credencial(TimeStampedModel, models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=True)
    servico = models.ForeignKey(ServicoCredencial, on_delete=models.PROTECT, null=True)

    class Meta:
        abstract = True


class CredencialDiscord(Credencial):
    discord_id = models.BigIntegerField(primary_key=True)
    discord_tag = models.CharField(max_length=100)
    avatar = models.CharField(max_length=100)
    public_flags = models.IntegerField()
    flags = models.IntegerField()
    locale = models.CharField(max_length=100)
    mfa_enabled = models.BooleanField()
    last_login = models.DateTimeField(null=True)

    class Meta:
        db_table = 'CREDENCIAL_DISCORD'
        verbose_name_plural = "Credenciais Discord"

    def __str__(self):
        return f'{self.servico.nome} - {self.discord_tag}'


class CredencialGoogle(Credencial):
    google_id = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    given_name = models.CharField(max_length=100)
    family_name = models.CharField(max_length=100)
    picture = models.URLField()
    locale = models.CharField(max_length=50)

    class Meta:
        db_table = 'CREDENCIAL_GOOGLE'
        verbose_name_plural = "Credenciais Google"

    def __str__(self):
        return f'{self.servico.nome} - {self.name}'
