import os

import requests
from django.shortcuts import redirect
from django.http import HttpResponse
from django.contrib.auth import login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from .models import *
from django.views.generic import TemplateView

# TODO criar .env e tirar dados hard coded


@login_required(login_url='./login')
def teste_de_autenticacao(request):
    return HttpResponse('Autenticado!')


class PaginaLogin(TemplateView):
    template_name = 'autenticacao/login.html'


def deslogar(request):
    logout(request)
    return redirect('login')


# Discord


def discord_login(request):
    auth_url_discord = os.environ.get('DISCORD_LOGIN_URL') or "https://discord.com/api/oauth2/authorize?client_id=828801740831850558&redirect_uri=http%3A%2F%2F127.0.0.1%3A8000%2Flogin%2Fdiscord%2Fredirect&response_type=code&scope=identify%20email"
    return redirect(auth_url_discord)


def discord_login_redirect(request):
    try:
        code = request.GET.get('code')
        usuario_discord = discord_exchange_code(code)

        usuario = User.objects.filter(email=usuario_discord['email']).first()

        if not usuario:
            usuario = User.objects.create_user(
                username=usuario_discord['username'],
                password=usuario_discord["username"],
                email=usuario_discord["email"]
            )

        credencial_discord = usuario.credencialdiscord_set.first()

        if not credencial_discord:
            servico_de_credencial, criado = ServicoCredencial.objects.get_or_create(nome='Discord')

            credencial_discord = CredencialDiscord.objects.create(
                discord_id=usuario_discord['id'],
                avatar=usuario_discord['avatar'],
                public_flags=usuario_discord['public_flags'],
                flags=usuario_discord['flags'],
                locale=usuario_discord['locale'],
                mfa_enabled=usuario_discord['mfa_enabled'],
                discord_tag=f'{usuario_discord["username"]}#{usuario_discord["discriminator"]}',
                user=usuario,
                servico=servico_de_credencial,
            )

        login(request, usuario)

    except Exception as error:
        print('\t\t\tERRO DE LOGIN DO DISCORD:', error)
        return HttpResponse(status=422)

    return redirect('estoque-principal')


def discord_exchange_code(code):
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    payload = {
        'client_id': os.environ.get('DISCORD_CLIENT_ID') or '828801740831850558',
        'client_secret': os.environ.get('DISCORD_CLIENT_SECRET') or '9G78m5G4CisGLFpWZXspQwoBH9fU07EU',
        'grant_type': 'authorization_code',
        'code': code,
        'redirect_uri': os.environ.get('DISCORD_LOGIN_REDIRECT_URL') or 'http://127.0.0.1:8000/login/discord/redirect',
        'scope': 'identify'
    }
    print('///')
    print('\t\t\t', payload)
    print('///')
    response = requests.post('https://discord.com/api/oauth2/token', data=payload, headers=headers)
    response.raise_for_status()

    credentials = response.json()
    access_token = credentials['access_token']

    response = requests.get('https://discord.com/api/v6/users/@me', headers={'Authorization': f'Bearer {access_token}'})
    response.raise_for_status()

    return response.json()


# Google


def google_login(request):
    auth_url_google = os.environ.get('GOOGLE_LOGIN_URL') or "https://accounts.google.com/o/oauth2/v2/auth?" \
        "scope=https%3A//www.googleapis.com/auth/userinfo.email" \
        "&access_type=offline" \
        "&include_granted_scopes=true" \
        "&response_type=code" \
        "&state=done" \
        f"&redirect_uri={os.environ.get('GOOGLE_LOGIN_REDIRECT_URL') or 'http%3A//localhost:8000/login/google/redirect'}" \
        "&client_id=623490611281-aqkacod4vka19gmma0fotukd1ilq2m17.apps.googleusercontent.com"
    return redirect(auth_url_google)


def google_login_redirect(request):
    try:
        code = request.GET.get('code')
        usuario_google = google_exchange_code(code)

        usuario = User.objects.filter(email=usuario_google['email']).first()

        if not usuario:
            usuario = User.objects.create_user(
                username=usuario_google['name'],
                password=usuario_google["name"],
                email=usuario_google["email"]
            )

        credencial_google = usuario.credencialgoogle_set.first()

        if not credencial_google:
            servico_de_credencial, criado = ServicoCredencial.objects.get_or_create(nome='Google')

            credencial_google = CredencialGoogle.objects.create(
                google_id=usuario_google['sub'],
                name=usuario_google["name"],
                given_name=usuario_google["given_name"],
                family_name=usuario_google["family_name"],
                picture=usuario_google["picture"],
                locale=usuario_google["locale"],
                user=usuario,
                servico=servico_de_credencial
            )

        login(request, usuario)

    except Exception as error:
        print('\t\t\tERRO DE LOGIN DO GOOGLE:', error)
        return HttpResponse(status=422)

    return redirect('estoque-principal')


def google_exchange_code(code):
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    querystring_payload = \
        f"code={code}" \
        f"&client_id=623490611281-aqkacod4vka19gmma0fotukd1ilq2m17.apps.googleusercontent.com" \
        f"&client_secret=1Aa6BYpcsvLUuEByrA2ZkML9" \
        f"&redirect_uri={os.environ.get('GOOGLE_LOGIN_REDIRECT_URL') or 'http%3A//localhost:8000/login/google/redirect'}" \
        "&grant_type=authorization_code"

    response = requests.post(f'https://oauth2.googleapis.com/token?{querystring_payload}', headers=headers)
    response.raise_for_status()

    credentials = response.json()
    access_token = credentials['access_token']

    response = requests.get(f'https://www.googleapis.com/oauth2/v3/userinfo', headers={'Authorization': f'Bearer {access_token}'})
    response.raise_for_status()

    return response.json()