from django.urls import path, include
from django.shortcuts import redirect
from .views import *

urlpatterns = [
    path('/sair', deslogar, name='logout'),

    path('', PaginaLogin.as_view(), name='login'),

    path('/discord', discord_login, name='login_discord'),
    path('/discord/redirect', discord_login_redirect),
    path('/discord/teste', teste_de_autenticacao, name='login_teste'),

    path('/google', google_login, name='login_google'),
    path('/google/redirect', google_login_redirect),
]
