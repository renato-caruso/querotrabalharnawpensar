from django.contrib.auth.models import User, Group
from .models import *
from rest_framework import serializers


class ProdutoSerializer(serializers.ModelSerializer):
    categoria = serializers.StringRelatedField()

    class Meta:
        model = Produto
        fields = ['url', 'id', 'nome', 'preco', 'quantidade', 'categoria']


class ProdutoLoteCompraSerializer(serializers.ModelSerializer):
    produto = ProdutoSerializer()

    class Meta:
        model = ProdutoLoteCompra
        fields = ['url', 'id', 'produto', 'lote', 'preco_anterior', 'quantidade', 'preco', 'preco_novo', 'custo'] # TODO custo dando erro no datatables do drf porque é uma porp em vez de coluna


class LoteCompraSerializer(serializers.ModelSerializer):
    produtos_comprados = ProdutoLoteCompraSerializer(many=True)

    class Meta:
        model = LoteCompra
        fields = ['url', 'id', 'abertura', 'fechamento', 'produtos_comprados', 'custo'] # TODO custo dando erro no datatables do drf porque é uma porp em vez de coluna

# 20217396039943
