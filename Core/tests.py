from django.test import TestCase
from Core.models import *
import datetime


class ModelsTest(TestCase):

    def test_criou_categoria(self):
        categoria = ProdutoCategoria(nome="Bebidas Lacteas")
        categoria.save()

        self.assertIsNotNone(categoria.id)

    def test_criou_produto(self):
        self.test_criou_categoria()

        produto = Produto(nome="Itambe 5lt", categoria=ProdutoCategoria.objects.first(), quantidade=5)
        produto.save()

        self.assertIsNotNone(produto.id)

    def test_criou_status_de_lote_de_compra(self):
        lote_status = LoteCompraStatus(nome="Recebido")
        lote_status.save()

        self.assertIsNotNone(lote_status.id)

    def test_criou_lote_de_compra(self):
        self.test_criou_status_de_lote_de_compra()

        lote = LoteCompra(fechamento=datetime.date.today(), status=LoteCompraStatus.objects.first())
        lote.save()

        self.assertIsNotNone(lote.id)

    def test_criou_produto_de_lote_compra(self):
        self.test_criou_lote_de_compra()
        self.test_criou_produto()

        produto_lote = ProdutoLoteCompra()
        produto_lote.lote = LoteCompra.objects.first()
        produto_lote.produto = Produto.objects.first()
        produto_lote.preco_anterior = 0
        produto_lote.quantidade = 10
        produto_lote.preco = 5
        produto_lote.preco_novo = produto_lote.calcular_preco()

        produto_lote.produto.preco = produto_lote.preco_novo

        produto_lote.save()
        produto_lote.produto.save()

        self.assertIsNotNone(produto_lote.id)

    def test_deve_recalcular_preco(self):
        produto = Produto()
        produto.preco = 10
        produto.quantidade = 3
        # saldo do custo em estoque é R$ 30 ...

        produto_lote = ProdutoLoteCompra()
        produto_lote.produto = produto

        produto_lote.preco_anterior = 10
        produto_lote.preco = 35
        produto_lote.quantidade = 2
        # saldo do custo da compra é R$ 70 ...

        self.assertEqual(produto_lote.calcular_preco(), 20)
        # saldo do estoque (30) + saldo da compra (70) = R$ 100 ...
        # novo estoque = quantidade estocada + quantidade comprada ...
        # saldo total (100) / novo estoque (5) = R$ 20
