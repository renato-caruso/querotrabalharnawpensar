from rest_framework import viewsets, permissions
from .serializers import ProdutoSerializer, ProdutoLoteCompraSerializer, LoteCompraSerializer
from .models import Produto, ProdutoLoteCompra, LoteCompra
from .filters import DynamicSearchFilter
from rest_framework_datatables.filters import DatatablesFilterBackend

from datetime import datetime
from django.db import transaction

from rest_framework.response import Response


class ProdutoViewSet(viewsets.ModelViewSet):
    queryset = Produto.objects.all()
    serializer_class = ProdutoSerializer
    permission_classes = [permissions.DjangoModelPermissions]
    filter_backends = [DynamicSearchFilter, DatatablesFilterBackend]


class ProdutoLoteCompraViewSet(viewsets.ModelViewSet):
    queryset = ProdutoLoteCompra.objects.all()
    serializer_class = ProdutoLoteCompraSerializer
    model = ProdutoLoteCompra
    permission_classes = [permissions.DjangoModelPermissions]
    filter_backends = [DynamicSearchFilter, DatatablesFilterBackend]


class LoteCompraViewSet(viewsets.ModelViewSet):
    queryset = LoteCompra.objects.all()
    serializer_class = LoteCompraSerializer
    permission_classes = [permissions.DjangoModelPermissions]
    filter_backends = [DynamicSearchFilter, DatatablesFilterBackend]

    def create(self, request, *args, **kwargs):
        fechamento = request.data.get('fechamento')
        produtos_comprados = request.data.get('produtos_comprados', [])

        try:
            datetime.strptime(fechamento, "%Y-%m-%dT%H:%M:%S.%fZ")
        except Exception as error:
            return Response(data={'fechamento': 'Data de fechamento ISO inválida.'}, status=422)

        try:
            with transaction.atomic():
                try:
                    lote = LoteCompra.objects.create(fechamento=fechamento)

                    for produto_comprado in produtos_comprados:

                        produto_lote_compra = ProdutoLoteCompra(**{
                            'quantidade': produto_comprado['quantidade'],
                            'preco': produto_comprado['preco'],
                        })

                        produto = Produto.objects.get(id=produto_comprado['id'])
                        produto_lote_compra.produto = produto

                        produto_lote_compra.preco_anterior = produto.preco
                        produto_lote_compra.preco_novo = produto_lote_compra.calcular_preco()
                        produto.preco = produto_lote_compra.preco_novo
                        produto.quantidade += produto_lote_compra.quantidade
                        produto_lote_compra.lote = lote

                        produto_lote_compra.save()
                        produto.save()

                except Exception as error:
                    raise Exception('Nesse try, validações especificas!') from error

        except Exception as error:
            transaction.rollback()
            return Response(data={'non_field_error': 'Erro geral.'}, status=422)

        return Response(data=LoteCompraSerializer(lote, context={'request': request}).data, status=201)

    def comprar_produto(self, lote, id_produto):
        pass