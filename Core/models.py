from django.db import models
from django_extensions.db.models import TimeStampedModel


class ProdutoCategoria(TimeStampedModel, models.Model):
    nome = models.TextField(max_length=50)

    class Meta:
        db_table = 'PRODUTO_CATEGORIA'
        verbose_name_plural = "Categorias de Produto"

    def __str__(self):
        return self.nome


class Produto(TimeStampedModel, models.Model):
    nome = models.TextField(max_length=50, unique=True)
    preco = models.FloatField(default=0)
    categoria = models.ForeignKey(ProdutoCategoria, null=True, on_delete=models.PROTECT)
    quantidade = models.PositiveIntegerField(default=0)

    class Meta:
        db_table = 'PRODUTO'

    def __str__(self):
        return self.nome


class LoteCompraStatus(TimeStampedModel, models.Model):
    nome = models.TextField(max_length=50)

    class Meta:
        db_table = 'LOTE_COMPRA_STATUS'
        verbose_name_plural = "Status de Lotes de Compra"

    def __str__(self):
        return self.nome


class LoteCompra(TimeStampedModel, models.Model):
    abertura = models.DateTimeField(auto_now_add=True)
    fechamento = models.DateTimeField(blank=True)
    status = models.ForeignKey(LoteCompraStatus, null=True, on_delete=models.PROTECT)

    @property
    def custo(self):
        return sum([produto_lote_compra.preco * produto_lote_compra.quantidade for produto_lote_compra in self.produtos_comprados.all()])

    class Meta:
        db_table = 'LOTE_COMPRA'
        verbose_name_plural = "Lotes de Compra"

    def __str__(self):
        return f'{self.id} - {self.fechamento.day}/{self.fechamento.month}/{self.fechamento.year}'


class ProdutoLoteCompra(TimeStampedModel, models.Model):
    produto = models.ForeignKey(Produto, on_delete=models.PROTECT)
    lote = models.ForeignKey(LoteCompra, related_name='produtos_comprados', on_delete=models.PROTECT)
    preco_anterior = models.FloatField(default=0)
    quantidade = models.IntegerField()
    preco = models.FloatField()
    preco_novo = models.FloatField()

    @property
    def custo(self):
        return self.quantidade * self.preco

    def calcular_preco(self):
        estoque_anterior = self.produto.quantidade
        custo_da_compra = self.preco * self.quantidade
        saldo_custo_anterior = estoque_anterior * self.preco_anterior
        novo_estoque = estoque_anterior + self.quantidade
        return (custo_da_compra + saldo_custo_anterior) / novo_estoque

    '''def estoque(self):
        preco_da_compra = self.preco * self.quantidade
        return abs((self.preco_novo - preco_da_compra) / self.preco_anterior)'''

    class Meta:
        db_table = 'PRODUTO_LOTE_COMPRA'
        verbose_name_plural = "Produtos de Lote de Compra"

    def __str__(self):
        return f'Lote: {self.lote_id} - Nome: {self.produto.nome}'
