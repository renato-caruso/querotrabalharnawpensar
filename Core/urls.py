from django.urls import include, path
from rest_framework import routers
from .views import *

router = routers.DefaultRouter()
router.register(r'produtos', ProdutoViewSet)
router.register(r'produtos_comprados', ProdutoLoteCompraViewSet)
router.register(r'lotes_de_compra', LoteCompraViewSet)

urlpatterns = [
    path('/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
