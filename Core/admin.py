from django.contrib import admin
from Core.models import *
from Autenticacao.models import *

# Autenticação
admin.site.register(ServicoCredencial)
admin.site.register(CredencialDiscord)
admin.site.register(CredencialGoogle)

# Core
admin.site.register(ProdutoCategoria)
admin.site.register(Produto)
admin.site.register(LoteCompraStatus)
admin.site.register(LoteCompra)
admin.site.register(ProdutoLoteCompra)

