from django.contrib import admin
from django.urls import path, include, re_path
from django.http import HttpResponse
from django.shortcuts import redirect

urlpatterns = [
    # admin
    path('admin', admin.site.urls),

    path('api', include('Core.urls')),
    # raiz
    path('', lambda request: redirect('estoque-principal')),

    # credenciais
    path('login', include('Autenticacao.urls'), name='login'),

    # modulos
    path('estoque', include('Estoque.urls')),

    # default
    re_path(r'^.', lambda request: HttpResponse('<h1>404</h1>')),
]

