from django.urls import path
from .views import *


urlpatterns = [
    path('/', PaginaPrincipal.as_view(), name='estoque-principal'),

    path('/compra', PaginaDeCompra.as_view(), name='estoque-compra'),
    path('/compra/formulario', PaginaDeCargaPorFormulario.as_view(), name='estoque-compra-formulario'),
    path('/compra/arquivo', PaginaDeCargaPorArquivo.as_view(), name='estoque-compra-arquivo'),
    path('/compra/arquivo/modelo', download_do_arquivo_modelo, name='estoque-compra-arquivo_modelo'),

    path('/cadastro/produtos', PaginaCadastroProduto.as_view(), name='estoque-cadastro-produto'),

    path('/cadastro/lotes_compras', PaginaListaLotesDeCompra.as_view(), name='estoque-cadastro-lote_de_compra'),
    path('/lote_compra/listar', AJAXListaLotesDeCompra.as_view(), name='lote_de_compra-listar'),
]
