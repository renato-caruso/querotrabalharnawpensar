from django.views.generic import *
from .forms import *
from .services import *
from django.views.generic.edit import CreateView
from django.forms import formset_factory
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.shortcuts import redirect


class PaginaPrincipal(TemplateView):
    template_name = 'estoque/base.html'


class PaginaDeCompra(TemplateView):
    template_name = 'estoque/compra/base.html'


class PaginaDeCargaPorFormulario(SuccessMessageMixin, FormView):
    template_name = 'estoque/compra/carga_formulario.html'
    form_class = formset_factory(ProdutoLoteForm, extra=0, min_num=1, validate_min=True)
    success_url = '../compra'
    success_message = "Carga realizada!"

    def form_valid(self, form):
        form_set = form
        servico_de_carga = ServicoCargaCompra()

        if not servico_de_carga.processar_formulario(form_set):
            print([form.errors for form in form_set.forms])
            return self.form_invalid(form_set)

        messages.success(request=self.request, message=self.success_message)
        return redirect(self.success_url)


class PaginaDeCargaPorArquivo(SuccessMessageMixin, FormView):
    template_name = 'estoque/compra/carga_arquivo.html'
    form_class = ArquivoForm
    success_url = '/estoque/compra'
    success_message = "Carga realizada!"

    def post(self, request):
        form_arquivo = ArquivoForm(request.POST, request.FILES)
        caminho_arquivo = request.FILES['arquivo_de_carga'].temporary_file_path()
        servico_de_carga = ServicoCargaCompra()

        if not form_arquivo.is_valid():
            return self.form_invalid(form_arquivo)

        if not servico_de_carga.processar_arquivo_csv(caminho_arquivo):
            [form_arquivo.add_error('arquivo_de_carga', forms.ValidationError(mensagem)) for mensagem in servico_de_carga.erros]
            print(form_arquivo.errors)
            return self.form_invalid(form_arquivo)

        return self.form_valid(form_arquivo)


def download_do_arquivo_modelo(request):
    return ServicoCargaCompra.gerar_modelo_de_arquivo()


class PaginaCadastroProduto(SuccessMessageMixin, CreateView):
    template_name = "estoque/cadastro/produto.html"
    model = Produto
    fields = ['nome']
    success_url = '../../'
    success_message = "Produto cadastrado!"


class PaginaListaLotesDeCompra(TemplateView):
    template_name = 'estoque/cadastro/lote_de_compra.html'


class AJAXListaLotesDeCompra(BaseDatatableView):
    model = LoteCompra
