from django import forms
from django.core.exceptions import ValidationError
from Core.models import Produto


class ProdutoLoteForm(forms.Form):
    nome = forms.CharField(label='Nome do Produto', required=False)
    quantidade = forms.CharField(label='Quantidade', required=False)
    preco = forms.CharField(label='Preço Unitario', required=False)

    def is_valid(self):
        return True


class ArquivoForm(forms.Form):
    arquivo_de_carga = forms.FileField()

    def clean_arquivo_de_carga(self):
        arquivo_de_carga = self.cleaned_data.get("arquivo_de_carga")

        if not str(arquivo_de_carga).endswith('.csv'):
            raise ValidationError('Formato inválido!')

        return self.cleaned_data
