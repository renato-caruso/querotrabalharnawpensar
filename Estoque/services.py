from django.utils.translation import gettext as _
from django.db import transaction
from django.http import HttpResponse
from Core.models import *
from django.forms import ValidationError
from datetime import datetime
import csv


class ErroCargaCompra(Exception):
    def __init__(self, mensagem, erros=None):
        super().__init__(mensagem)
        self.erros = erros


class ServicoCargaCompra():

    def __init__(self):
        self.erros = []
        self.invalido = False

    def processar_formulario(self, produtos_lote_compra_formset):
        self.erros = []

        try:
            try:
                with transaction.atomic():

                    lote = LoteCompra(fechamento=datetime.today())
                    lote.save()
                    produto_lote = ProdutoLoteCompra()
                    produto_lote.lote = lote

                    novos_registros = []

                    for index, formulario_produto_compra in enumerate(produtos_lote_compra_formset):

                        [produto_formulario_nome, produto_formulario_quantidade, produto_formulario_preco] = [formulario_produto_compra['nome'].data, formulario_produto_compra['quantidade'].data, formulario_produto_compra['preco'].data]

                        quantidade_valida, mensagem = self.valida_quantidade(produto_formulario_quantidade)
                        if not quantidade_valida:
                            formulario_produto_compra.add_error('quantidade', mensagem)
                        else:
                            produto_lote.quantidade = int(produto_formulario_quantidade)

                        preco_valido, mensagem = self.valida_preco(produto_formulario_preco)
                        if not preco_valido:
                            formulario_produto_compra.add_error('preco', mensagem)
                        else:
                            produto_lote.preco = float(produto_formulario_preco)

                        produto = None
                        nome_valido, mensagem = self.valida_nome(produto_formulario_nome)
                        if not nome_valido:
                            formulario_produto_compra.add_error('nome', mensagem)
                        else:
                            produto = Produto.objects.filter(nome=produto_formulario_nome).first()

                        if not produto:
                            formulario_produto_compra.add_error(None, f'Produto ainda não foi cadastrado!')
                            self.invalido = True
                        else:
                            produto_lote.produto = produto

                            produto_lote.preco_anterior = produto.preco
                            produto_lote.preco_novo = produto_lote.calcular_preco()
                            produto.preco = produto_lote.preco_novo
                            produto.quantidade += produto_lote.quantidade

                            novos_registros.extend([produto_lote, produto])

                    if self.invalido: raise ValidationError('Dados inválidos!')

                    [registro.save() for registro in novos_registros]

                    return True

            except ValueError as error:
                raise ErroCargaCompra(error) from error

            except ValidationError as error:
                raise ErroCargaCompra(error) from error

        except ErroCargaCompra as error:
            transaction.rollback()
            self.erros.append(error)
            return False

    def processar_arquivo_csv(self, caminho_arquivo):
        self.erros = []

        arquivo_csv = csv.reader(open(caminho_arquivo, 'r'))

        try:
            try:
                with transaction.atomic():

                    lote = LoteCompra(fechamento=datetime.today())
                    lote.save()

                    index = 0
                    cabecalho = next(arquivo_csv)
                    possui_linhas = False

                    for linha in arquivo_csv:
                        index += 1
                        possui_linhas = True

                        if not len(linha) == 3: raise ValueError(f'Quantidade inválida de colunas na linha {index}!')

                        [produto_formulario_nome, produto_formulario_quantidade, produto_formulario_preco] = linha

                        produto_lote = ProdutoLoteCompra()
                        produto = Produto.objects.filter(nome=produto_formulario_nome).first()
                        print('\t\t-->', produto_formulario_nome, produto)
                        if not produto: raise ValueError(f'Produto "{produto_formulario_nome}" não cadastrado na linha {index}!')

                        produto_lote.lote = lote
                        produto_lote.produto = produto

                        if not produto_formulario_quantidade.isdigit(): raise ValueError(f'A quatidade "{produto_formulario_quantidade}" na linha {index} não é um digito válido!')
                        produto_lote.quantidade = int(produto_formulario_quantidade)

                        try: float(produto_formulario_preco)
                        except: raise ValueError(f'O preço "{produto_formulario_preco}" na linha {index} não é um digito válido!')
                        produto_lote.preco = float(produto_formulario_preco)

                        produto_lote.preco_anterior = produto_lote.produto.preco
                        produto_lote.preco_novo = produto_lote.calcular_preco()
                        produto_lote.produto.preco = produto_lote.preco_novo
                        produto_lote.produto.quantidade += produto_lote.quantidade

                        produto_lote.produto.save()
                        produto_lote.save()

                    if not possui_linhas: raise ValueError(f'Sem dados para importação.')


            except ValueError as error:
                raise ErroCargaCompra(error) from error

        except ErroCargaCompra as error:
            transaction.rollback()
            self.erros.append(error)
            return False

        return True

    def valida_nome(self, nome, raise_error=None):
        if nome is None or nome == '':
            self.invalido = True
            return False, f'O nome não pode ser vazio!'
        if len(nome) < 3 or nome.isdigit():
            self.invalido = True
            return False, f'O nome "{nome}" é inválido. Deve possuir mais de duas letras!'
        return True, None

    def valida_quantidade(self, quantidade, raise_error=None):
        if quantidade is None or quantidade == '':
            self.invalido = True
            return False, f'A quantidade não pode ser vazia!'
        if not quantidade.isdigit() or float(quantidade) <= 0 or float(quantidade) != int(quantidade):
            self.invalido = True
            return False, f'A quantidade "{quantidade}" é inválida. Deve ser um número inteiro positivo!'
        return True, None

    def valida_preco(self, preco, raise_error=None):
        if preco is None or preco == '':
            self.invalido = True
            return False, f'O preco não pode ser vazio!'
        if not preco.isdigit() or float(preco) <= 0:
            self.invalido = True
            return False, f'O preço "{preco}" é inválido. Deve ser um número positivo!'
        return True, None

    @staticmethod
    def gerar_modelo_de_arquivo():
        response = HttpResponse(content_type='text/csv; charset=utf-8')
        response['Content-Disposition'] = 'attachment; filename="Modelo_de_Carga.csv"'

        response.write(u'\ufeff'.encode('utf8'))
        writer = csv.writer(response)
        writer.writerow(['Nome', 'Quantidade', 'Preço'])

        return response
